import cages.*;
import animals.*;
import java.util.Scanner;

public class JavariPark{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");
		String[] kindOfAnimal = {"cat", "lion", "eagle", "parrot", "hamster"};
		CageOfCages[] cageAnimal = new CageOfCages[kindOfAnimal.length];
		int[] totalEachAnimal = new int[5];

		for(int i=0; i<5; i++){
			System.out.printf("%s: ", kindOfAnimal[i]);
			int totalAnimal = Integer.parseInt(in.nextLine());
			
			totalEachAnimal[i] = totalAnimal;
			cageAnimal[i] = new CageOfCages(totalAnimal);
			
			if(totalAnimal != 0){
				System.out.printf("Provide the information of %s(s):\n", kindOfAnimal[i]);
				String[] dataAnimal = in.nextLine().split(",");
				
				for (int j=0 ; j<totalAnimal ; j++){
					Animals animal = null;	
					String[] eachDataAnimal = dataAnimal[j].split("\\|");

					if (i==0){
						animal = new Cat(eachDataAnimal[0], Integer.parseInt(eachDataAnimal[1]));
					} else if (i==1){
						animal = new Lion(eachDataAnimal[0], Integer.parseInt(eachDataAnimal[1]));
					} else if (i==2){
						animal = new Eagle(eachDataAnimal[0], Integer.parseInt(eachDataAnimal[1]));
					} else if (i==3){
						animal = new Parrot(eachDataAnimal[0], Integer.parseInt(eachDataAnimal[1]));
					} else {
						animal = new Hamster(eachDataAnimal[0], Integer.parseInt(eachDataAnimal[1]));
					}

					Cage cage = new Cage(animal);
					cageAnimal[i].setLevel(cage, j);
				}
			}
		}
		System.out.print("Animals have been successfully recorded!\n\n");
		System.out.println("=============================================");

		System.out.println("Cage arrangement:");
		
		for(int i=0; i < 5 ; i++){
			if(cageAnimal[i].getTotalAnimals()!=0)
				ArrangementOfCages.reArrangement(cageAnimal[i]);
		}

		System.out.println("NUMBER OF ANIMALS:");

		for(int i=0 ; i<5 ; i++){
			System.out.println(kindOfAnimal[i] + ":" + totalEachAnimal[i]);
			if (i==4){
				System.out.print("\n\n=============================================\n");
			}
		}

		while (true){
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: cat, 2: lion, 3: eagle, 4: parrot, 5: hamster, 99: Exit)");
			int chooseAnimal = Integer.parseInt(in.nextLine());

			if (chooseAnimal == 99){
				break;
			}

			System.out.printf("Mention the name %s of you want to visit: ", kindOfAnimal[chooseAnimal-1]);
			
			String animalName = in.nextLine();
			

			if (cageAnimal[chooseAnimal-1].findAnimal(animalName) == null){
				System.out.printf("There is no %s with that name! Back to the office!\n\n", kindOfAnimal[chooseAnimal-1]);
				continue;
			} else {
				System.out.printf("You are visiting %s (%s) now, what would you like to do?\n", animalName, 
									kindOfAnimal[chooseAnimal-1]);
			}

			Perintah(kindOfAnimal[chooseAnimal-1], animalName, cageAnimal[chooseAnimal-1]);
		}
	}

	public static void Perintah(String kindOfAnimal, String animalName, CageOfCages listOfCage){
		Scanner in = new Scanner (System.in);

		String option = "";
		if (kindOfAnimal.equals("cat")){
			option += "1: Brush the fur 2: Cuddle";
		} else if (kindOfAnimal.equals("eagle")) {
			option += "1: Order to fly";
		} else if (kindOfAnimal.equals("hamster")) {
			option += "1: See it gnawing 2: Order to run in the hamster wheel";
		}else if (kindOfAnimal.equals("lion")) {
			option += "1: See it hunting 2: Brush the mane 3: Disturb it";
		} else {
			option += "1: Order to fly 2: Do conversation";
		} 

		System.out.println(option);

		if (kindOfAnimal == "cat"){
			Cat aCat = new Cat(animalName, listOfCage.findAnimal(animalName).getAnimal().getLength());
			Animals newCat = (Animals) aCat;
			newCat.MakeVoice();
		} else if (kindOfAnimal == "eagle"){
			Eagle anEagle = new Eagle(animalName, listOfCage.findAnimal(animalName).getAnimal().getLength());
			Animals newEagle = (Animals) anEagle;
			newEagle.MakeVoice();
		} else if (kindOfAnimal == "hamster"){
			Hamster aHamster = new Hamster(animalName, listOfCage.findAnimal(animalName).getAnimal().getLength());
			Animals newHamster = (Animals) aHamster;
			newHamster.MakeVoice();
		} else if(kindOfAnimal == "lion"){
			Lion aLion = new Lion(animalName, listOfCage.findAnimal(animalName).getAnimal().getLength());
			Animals newLion = (Animals) aLion;
			newLion.MakeVoice();
		} else{
 			Parrot aParrot = new Parrot(animalName, listOfCage.findAnimal(animalName).getAnimal().getLength());
			Animals newParrot = (Animals) aParrot;
			newParrot.MakeVoice();
		}
		System.out.println("Back to the office!\n");
	}
}
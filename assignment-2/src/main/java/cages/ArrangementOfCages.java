package cages;

import java.util.ArrayList;
import cages.Cage;
import cages.CageOfCages;

public class ArrangementOfCages{
	
	public static void reArrangement(CageOfCages listOfCage){
		ArrayList<Cage> tempListCage = new ArrayList<Cage>();
		tempListCage = listOfCage.getFirstLevel();
		
		System.out.println("location: " + listOfCage.getFirstLevel().get(0).getLocation());
		printInfo(listOfCage);

		listOfCage.setFirstLevel(listOfCage.getThirdLevel());
		listOfCage.setThirdLevel(listOfCage.getSecondLevel());
		listOfCage.setSecondLevel(tempListCage);

		for(int i=0; i<3 ; i++){
			ArrayList<Cage> tempLevel = null;

			if (i==0){
				tempLevel = listOfCage.getFirstLevel();
			} else if (i==1){
				tempLevel = listOfCage.getSecondLevel();
			} else {
				tempLevel = listOfCage.getThirdLevel();
			}

			for (int j=0 ; j<tempLevel.size()/2 ; j++){
				Cage tempCage = tempLevel.get(j);
				tempLevel.set(j, tempLevel.get(tempLevel.size()-1-j));
				tempLevel.set(tempLevel.size()-1-j, tempCage);
			}
		}

		System.out.println("\nAfter rearrangement..." );
		printInfo(listOfCage);
	}

	public static void printInfo(CageOfCages listOfCage){
		for (int i=3; i>0 ; i--){
			ArrayList<Cage> tempLevel = null;
			if (i==3){
				tempLevel = listOfCage.getThirdLevel();
			} else if (i==2){
				tempLevel = listOfCage.getSecondLevel();
			} else {
				tempLevel = listOfCage.getFirstLevel();
			}

			System.out.print("level " + i + ": ");
			
			String tempInfo = "";
			for(Cage cage : tempLevel){
				tempInfo += cage.getAnimal().getName() + " ";
				tempInfo += "(" + cage.getAnimal().getLength() + " - " + cage.getType();
				tempInfo += ")" + ", ";
			}

			System.out.println(tempInfo);	
			if(i==1) System.out.println("\n");			 
		}
	}
}
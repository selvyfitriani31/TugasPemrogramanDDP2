package cages;

import java.util.ArrayList;
import cages.Cage;

public class CageOfCages {

	private ArrayList<Cage> firstLevel = new ArrayList<Cage>();
	private ArrayList<Cage> secondLevel = new ArrayList<Cage>();
	private ArrayList<Cage> thirdLevel = new ArrayList<Cage>();
	private int totalAnimals;

	public CageOfCages(int totalAnimals){
		this.totalAnimals = totalAnimals;
	}

	public void setFirstLevel(ArrayList<Cage> firstLevel){
		this.firstLevel = firstLevel;
	}

	public ArrayList<Cage> getFirstLevel(){
		return this.firstLevel;
	}

	public void setSecondLevel(ArrayList<Cage> secondLevel){
		this.secondLevel = secondLevel;
	}

	public ArrayList<Cage> getSecondLevel(){
		return this.secondLevel;
	}

	public void setThirdLevel(ArrayList<Cage> thirdLevel){
		this.thirdLevel = thirdLevel;
	}

	public ArrayList<Cage> getThirdLevel(){
		return this.thirdLevel;
	}

	public void setLevel(Cage cage, int index){
		int hasilBagi = totalAnimals / 3;
		
		if (hasilBagi == 0){
			hasilBagi ++;
		}

		if (index < hasilBagi) {
			firstLevel.add(cage);
		} else if (index < 2*hasilBagi){
			secondLevel.add(cage);
		} else if (hasilBagi==2 && index == 2 * (hasilBagi)){
			secondLevel.add(cage);
		} else {
			thirdLevel.add(cage);
		}
	}

	public Cage findAnimal(String animalName){
		Cage cageAnimal = null;
		for (int i=0; i<3; i++){
			ArrayList<Cage> tempLevel;
			if (i==0){
				tempLevel = firstLevel;
			} else if (i==1){
				tempLevel = secondLevel;
			} else {
				tempLevel = thirdLevel;
			}

			for (int j=0; j<tempLevel.size(); j++) {
				if (tempLevel.get(j).getAnimal().getName().equals(animalName)) {
					cageAnimal = tempLevel.get(j);
				}
			}
		}
		return cageAnimal;	
	}

	public int getTotalAnimals() {
		return this.totalAnimals;
	}
}
package cages;

import animals.*;

public class Cage {
	
	private Animals animal;
	private String type = "C";
	private String location;

	public Cage(Animals animal){
		this.animal = animal;

		if (!animal.getWild()){
			this.location = "indoor";
			if(animal.getLength() <  61){
				this.type = "B";
			} if (animal.getLength() < 45) {
				this.type = "A";
			}
		} else {
			this.location = "outdoor";
			if(animal.getLength() <  91){
				this.type = "B";
			} if (animal.getLength() < 75) {
				this.type = "A";
			}
		}
	}

	public Animals getAnimal() {
		return this.animal;
	}

	public String getType(){
		return this.type;
	}

	public String getLocation(){
		return this.location;
	}

	public String toString(){
		return " " + animal.getName() + "(" + animal.getLength() + " - " + this.type + ")";
	}

}
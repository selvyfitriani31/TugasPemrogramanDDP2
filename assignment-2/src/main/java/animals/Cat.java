package animals;

import java.util.Random;
import java.util.Scanner;

public class Cat extends Animals{
	public Cat(String name, int length){
		super(name, length, false);
	}

	public void MakeVoice(){
		Scanner in = new Scanner (System.in);
		Random r = new Random();
		String[] VoicesOfCat = {"Miaaaw..","Purrr..","Mwaw!", "Mraaawr!"};
		
		String userInput = in.nextLine();
		if (userInput.equals("1")){
			System.out.printf("Time to clean %s's fur\n", name);
			System.out.printf("%s makes a voice: Nyaaan...\n", name);
		} else if (userInput.equals("2")){
			System.out.printf("%s makes a voice: %s\n", name, VoicesOfCat[r.nextInt(4)]);
		} else {
			System.out.println("You do nothing...");
		}
	}

} 
package animals;

import java.util.Scanner;

public class Eagle extends Animals{
	public Eagle(String name, int length){
		super(name, length, true);
	}

	public void MakeVoice(){
		Scanner in = new Scanner(System.in);
		String userInput = in.nextLine();

		if(userInput.equals("1")){
			System.out.printf("%s makes a voice: kwaakk…\n", name);
			System.out.printf("You hurt!\n");
		} else {
			System.out.println("You do nothing...");
		}
	}
}
package animals;

import java.util.Scanner;

public class Parrot extends Animals{

	public Parrot(String name, int length){
		super(name, length, false);
	}

	public void MakeVoice(){
		Scanner in = new Scanner(System.in);
		String userInput = in.nextLine();

		if (userInput.equals("1")){
			System.out.printf("Parrot %s flies!\n", name);
			System.out.printf("%s makes a voice: FLYYYY…..\n", name);
		} else if (userInput.equals("2")){
			System.out.print("You say: ");
			String userVoice = in.nextLine();
			System.out.printf("%s says: %s\n", name, userVoice.toUpperCase());
		} else {
			System.out.println("You do nothing...");
		}
	}
}
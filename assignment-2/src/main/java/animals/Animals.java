package animals;

public class Animals{
	protected String name;
	protected int length;
	protected boolean wild;

	public Animals(String name, int length, boolean wild){
		this.name = name;
		this.length = length;
		this.wild = wild;
	}

	public void MakeVoice(){
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

	public void setLength(int panjang){
		this.length = length;
	}

	public int getLength(){
		return this.length;
	}

	public void setWild(boolean wild){
		this.wild = wild;
	}

	public boolean getWild(){
		return this.wild;
	}
}
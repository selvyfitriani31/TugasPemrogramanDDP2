package animals;

import java.util.Scanner;

public class Lion extends Animals{

	public Lion(String name, int length){
		super(name, length, true);
	}

	public void MakeVoice(){
		Scanner in = new Scanner(System.in);
		String userInput = in.nextLine();

		if(userInput.equals("1")){
			System.out.println("Lion is hunting..");
			System.out.printf("%s makes a voice: err...!\n", name);
		} else if (userInput.equals("2")){
			System.out.println("Clean the lion’s mane..");
			System.out.printf("%s makes a voice: Hauhhmm!\n", name);
		} else if (userInput.equals("3")){
			System.out.printf("%s makes a voice: HAUHHMM!\n", name);
		} else {
			System.out.println("You do nothing...");
		}
	}

}
package animals;

import java.util.Scanner;

public class Hamster extends Animals{

	public Hamster(String name, int length){
		super(name, length, false);
	}

	public void MakeVoice(){
		Scanner in = new Scanner(System.in);
		String userInput = in.nextLine();

		if (userInput.equals("1")){
			System.out.printf("%s makes a voice: ngkkrit.. ngkkrrriiit\n", name);
		} else if (userInput.equals("2")){
			System.out.printf("%s makes a voice: trrr…. trrr...\n", name);
		} else {
			System.out.println("You do nothing...");
		}
	}
}
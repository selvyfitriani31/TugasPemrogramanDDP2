package javari.animal;

public class Reptil extends Animal{
	private String tameReptiles;

	public Reptil (String[] input){
		super (Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]),
				Double.parseDouble(input[4]), Double.parseDouble(input[5]), Condition.parseCondition(input[7]));
		tameReptiles = input[6];	
	}

	public boolean specificCondition(){
		if (tameReptiles.equals("tame")) return false;
		return true;
	}
}
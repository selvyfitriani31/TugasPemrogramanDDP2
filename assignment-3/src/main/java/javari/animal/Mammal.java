package javari.animal;

public class Mammal extends Animal{

	private String pregnant;

	public Mammal (String[] input){
		super (Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]),
				Double.parseDouble(input[4]), Double.parseDouble(input[5]), Condition.parseCondition(input[7]));
		pregnant = input[6];
	}

	public boolean specificCondition(){
		if (getType().equals("Lion")){
			if (getGender().equals(Gender.FEMALE)) return true;
		} else {
			if (pregnant.equals("pregnant")) return true;
		}
		return false;
	}
}
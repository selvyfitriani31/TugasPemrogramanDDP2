package javari.animal;

public class Aves extends Animal{
	private String layingEggs;

	public Aves (String[] input){
		super (Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]),
				Double.parseDouble(input[4]), Double.parseDouble(input[5]), Condition.parseCondition(input[7]));
		layingEggs = input[6];	
	}

	public boolean specificCondition(){
		if (layingEggs.equals("laying eggs")) return true;
		return false;
	}
}
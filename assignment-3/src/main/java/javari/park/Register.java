package javari.park;
import javari.park.*;
import java.util.*;

public class Register implements Registration {

	private int RegistrationId;
	private String VisitorName;
	private ArrayList<SelectedAttraction> SelectedAttractions;

	public int getRegistrationId(){
		return this.RegistrationId;
	}

	public String setVisitorName(String name){
		this.VisitorName = name;
		return this.VisitorName;
	}
	public String getVisitorName(){
		return this.VisitorName;
	}

	public void setSelectedAttractions (ArrayList<SelectedAttraction> SelectedAttractions){
		this.SelectedAttractions = SelectedAttractions;
	}
	public List<SelectedAttraction> getSelectedAttractions(){
		return this.SelectedAttractions;
	}

    public boolean addSelectedAttraction(SelectedAttraction selected){
    	for(int i=0 ; i<SelectedAttractions.size() ; i++){
    		if (SelectedAttractions.get(i).equals(selected)) return  false;
    	}
    	return true;
    }
}
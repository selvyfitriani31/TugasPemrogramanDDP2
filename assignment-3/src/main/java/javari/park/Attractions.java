package javari.park;

import java.util.*;

import javari.animal.*;

public class Attractions implements SelectedAttraction {

	private String name;
	private String type;
	private ArrayList<Animal> performers;

	public Attractions (String name, String type){
		this.name = name;
		this.type = type;
	}

	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}

	public void setType(String type){
		this.type = type;
	}
	public String getType(){
		return this.type;
	}

	public void setPerformers(ArrayList<Animal> performers){
		this.performers = performers;
	}
	public ArrayList<Animal> getPerformers(){
		return this.performers;
	}

	public boolean addPerformer(Animal performer){
		if (performer.isShowable()) {
			performers.add(performer);
			return true;
		} return false;
	}

}
package javari.reader;
import javari.animal.*;
import java.io.*;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class RecordsReader extends CsvReader {
	private static long valid;
    //private ArrayList<Animal> ;
    public RecordsReader(Path file) throws IOException {
        super(file);
    }

    public List<String> getLines() {
        return lines;
    }

    private ArrayList<Animal> animals = new ArrayList<Animal>();


    public long countValidRecords(){
        /*
                        Asumsi untuk record valid :
                        1. id valid --> id > 0
                        2. tipe valid --> if tipe in ["Cat", "Lion", "Eagle", "Parrot", "Hamster", "Snake", "Whale"]
                        3. nama valid --> nama != ""
                        4. gender valid --> gender is "male" xor "female"
                        5. body length valid --> length > 0 tipe double
                        6. body weight valid --> weight > 0 tipe double
                        7. special status valid --> if != ""
                        8. condition valid --> if condition in ["healthy","not healthy"] 
                   */
        Integer id; String type; String name; Gender gender; double length;
        double weight; Condition condition;
        long valid = 0;
        for(int i=0; i<lines.size() ; i++){
            if (!lines.get(i).equals(null)){
                String[] record = lines.get(i).split(COMMA);
                if (isValidId(record[0]) && isValidTipe(record[1]) && isValidNama(record[2]) && isValidGender(record[3]) && 
                    isValidLength(record[4]) && isValidWeight(record[5]) && isValidSpecStatus(record[6], record[1]) && 
                    isValidCondition(record[7])) {
                    valid += 1;
                    Animal anAnimal;
                    if (record[1].equals("Cat") || record[1].equals("Lion") || record[1].equals("Whale") || record[1].equals("Hamster")){
                        anAnimal = new Mammal(record);
                    } else if (record[1].equals("Eagle") || record[1].equals("Parrot")) {
                        anAnimal = new Aves(record);
                    } else {
                        anAnimal = new Reptil(record);
                    }
                    animals.add(anAnimal);
                }
            }
        }
        return valid;
    }

    public long countInvalidRecords(){
        return (long) lines.size()-countValidRecords();
    }

    public boolean isValidId (String id){
        try {
            if (Integer.parseInt(id) > 0) return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean isValidTipe (String tipe){
        String[] tipeHewan = new String[7];
     	tipeHewan[0] = "Cat";
     	tipeHewan[1] = "Lion";
     	tipeHewan[2] = "Eagle";
     	tipeHewan[3] = "Parrot";
     	tipeHewan[4] = "Hamster";
     	tipeHewan[5] = "Snake";
     	tipeHewan[6] = "Whale";
   
        for (int i=0 ; i<7 ; i++){
            if (tipeHewan[i].equals(tipe)){
                return true;
            }
        }
        return false;
    }

    public boolean isValidNama (String nama){
        if (!nama.equals(null)) return true;
        return false;
    }


    public boolean isValidGender (String gender){
        if (gender.equals("male")) return true;
        else if (gender.equals("female")) return true;
        return false;
    }


    public boolean isValidLength (String length){
        try {
            if (Double.parseDouble(length) > 0.0) return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean isValidWeight (String weight){
        try {
            if (Double.parseDouble(weight) > 0.0) return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean isValidSpecStatus (String specialStatus, String tipe){
        if (specialStatus.equals("")) return true;
        if (tipe.equals("Hamster") || tipe.equals("Lion") || tipe.equals("Cat") || tipe.equals("Whale")){
            if (specialStatus.equals("pregnant")) return true;
        } else if (tipe.equals("Eagle") || tipe.equals("Parrot")) {
            if (specialStatus.equals("laying eggs")) return true;
        } else {
            if (specialStatus.equals("wild") || specialStatus.equals("tame")) return true;
        }
        return false;
    }

    public boolean isValidCondition (String condition){
        if (condition.equals("healthy") || condition.equals("not healthy")) return true;
        return false;
    }    

}
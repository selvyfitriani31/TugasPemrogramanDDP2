package javari.reader;
import javari.park.*;
import java.io.*;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class AttractionsReader extends CsvReader {
	public AttractionsReader(Path file) throws IOException {
        super(file);
    }

    public List<String> getLines() {
        return lines;
    }

    private ArrayList<Attractions> attractions = new ArrayList<Attractions>();
    public ArrayList<Attractions> getAtraksi() {
    	return attractions;
    }

    public boolean isValidAttraction(String tipe, String attraction){
    	if (attraction.equals("Circles of Fires")) {
    		if (tipe.equals("Lion") || tipe.equals("Whale") || tipe.equals("Eagle")) 
    		return true;
    	} else if (attraction.equals("Dancing Animals")) {
    		if (tipe.equals("Cat") || tipe.equals("Snake") || tipe.equals("Parrot") || tipe.equals("Hamster")) return true;
    	} else if (attraction.equals("Counting Masters")) {
    		if (tipe.equals("Whale") || tipe.equals("Hamster") || tipe.equals("Parrot")) return true;
    	} else if (attraction.equals("Passionate Coders")){
    		if (tipe.equals("Hamster") || tipe.equals("Snake") || tipe.equals("Cat")) return true;
    	}
    	return false;
    }

    public long countValidRecords(){
    	int valid = 0;
    	Set<String> validAttractionSet = new HashSet<String>();
    	for (int i=0; i<lines.size() ; i++){
   			if (!lines.get(i).equals(null)){
   				String[] record = lines.get(i).split(COMMA);
   				if (isValidAttraction(record[0], record[1])) {
   					validAttractionSet.add(record[1]);
   					Attractions attraction = new Attractions (record[1], record[0]);
   					attractions.add(attraction);
   					valid+=1;
   				}
   			}
   		} 		
   		return (long) validAttractionSet.size();
   	}
    public long countInvalidRecords(){
    	return (long) 0;
    }
}
package javari.reader;
import java.io.*;
import java.util. *;
import java.nio.file.Files;
import java.nio.file.Path;

public class CategoriesReader extends CsvReader {

	public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    public List<String> getLines() {
        return lines;
    }

    public boolean isValidCategorie (String tipe, String categorie) {
    	if (categorie.equals("mammals")) {
    		if (tipe.equals("Cat") || tipe.equals("Hamster") || tipe.equals("Lion") || tipe.equals("Whale")) return true;
    	} else if (categorie.equals("aves")) {
    		if (tipe.equals("Eagle") || tipe.equals("Parrot")) return true;
    	} else if (categorie.equals("reptiles")){
    		if (tipe.equals("Snake")) return true;
    	}
    	return false;
    }

    public long countValidRecords(){
    	/* 
            Asumsi untuk categorie valid : tipe hewan sesuai dengan tipe kategori   
        */        
    	// Set<String> validCategoriesSet = new HashSet<String>();
   
     //    for (int i=0 ; i<5 ; i++){
     //    	if (!lines.get(i).equals(null)) {
     //    		String[] record = lines.get(i).split(COMMA);
     //    		if (isValidCategorie(record[0], record[1])) validCategoriesSet.add(record[1]);
     //    	}
     //    }
        
        return (long) 0;
    }

    public long countInvalidRecords(){
        return (long) 0;
    }

    public boolean isValidSection(String tipe, String categorie, String section){
    	if (categorie.equals("mammals") && section.equals("Explore the Mammals")) {
    		if (isValidCategorie(tipe, categorie)) return true;
    	} else if (categorie.equals("aves") && section.equals("World of Aves")) {
    		if (isValidCategorie(tipe, categorie)) return true;
    	} else if (categorie.equals("reptiles") && section.equals("Kingdom Reptillian")) {
    		if (isValidCategorie(tipe, categorie)) return true;
    	}
    	return false;
    }

    public long countValidSections(String tipe, String categorie, String section){
    	/*
    	Asumsi section valid : kategori yang bersesuaian dengan tipe juga bersesuaian denngan section.
   		*/
   		Set<String> validSectionSet = new HashSet<String>();
   		for (int i=0 ; i<lines.size() ; i++){
   			String[] record = lines.get(i).split("COMMA");
   			if (isValidSection(record[0], record[1], record[2])) validSectionSet.add(record[2]);
   		}
   		return (long) validSectionSet.size();  				
    }

    public long countInvalidSections(){
    	return (long) 0;
    }

}
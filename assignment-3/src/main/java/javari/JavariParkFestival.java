import java.util.Scanner;
import java.nio.file.Files;
import java.nio.file.Path;
import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;

public class JavariParkFestival {

	private static CsvReader[] CsvFiles = new CsvReader[3];

	public static void main (String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");

		System.out.print("Please provide the source data path: ");

		String source = in.nextLine();

		String textPath = System.getProperty("user.dir", "\\input");

		while (true){
			try {
				// CsvFiles[0] = AttractionsReader(source.get(textPath, "animals_attractions.csv"));
				// CsvFiles[1] = CategoriesReader(source.get(textPath, "animals_categories.csv"));
    //             CsvFiles[2] = RecordsReader(source.get(textPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
			} catch (Exception e) {
				System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                textPath = textPath.replace("\\", "\\\\");
                System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
				System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
			}
		}
	}

		// System.out.printf("Found _%s_ valid sections and _%s_ invalid sections\n", CsvFiles[1].countValidSections(), 
		// 				CsvFiles[1].countInvalidSections());
		// System.out.printf("Found _%s_ valid attractions and _%s_ invalid attractions\n", CsvFiles[0].countValidAttractions(),
		// 				CsvFiles[0].countInvalidAttractions());
		// System.out.printf("Found _%s_ valid animal categories and _%s_ invalid animal categories\n", CsvFiles[1].countValidCategories(),
		// 				CsvFiles[1].countInvalidCategories());
		// System.out.printf("Found _%s_ valid animal records and _%s_ invalid animal records\n", CsvFiles[2].countValidRecords(), 
		// 				CsvFiles[2].countInvalidRecords());

		
		// introJavariPark()
	
	
	public void introJavariPark(){
		Scanner in = new Scanner(System.in);
		System.out.println("Javari Park has 3 sections:\n1. Explore the Mammals\n2. World of Aves\n3. Reptilian Kingdom\n");
		System.out.print("Please choose your preferred section (type the number): ");
		String choosen = in.nextLine();
		if (choosen.equals("1")) chooseMammals();
		else if (choosen.equals("2")) chooseAves();
		else if (choosen.equals("3")) chooseReptilles();
		else if (choosen.equals("#")) introJavariPark(); 
		else {
			System.out.println("your input is invali\n");
			introJavariPark();
		}
	}


	public void chooseMammals(){
		Scanner in = new Scanner(System.in);
		System.out.println("\n--Explore the Mammals--\n1. Hamster\n2. Lion\n3. Cat\n4. Whale");
		System.out.print("Please choose your preferred animals (type the number): ");
		String choosen = in.nextLine();
		String choosen2;
		if (choosen.equals("1")) {
			System.out.println("\n---Hamster---");
			System.out.println("Attractions by Hamster:");
			System.out.println("1. Dancing Animals\n2. Counting Masters\n3. Passionate Coders");
			System.out.print("Please choose your preferred attractions (type the number): ");
			choosen2 = in.nextLine();
			if((choosen2.equals("1") && adaAtraksi("Hamster", "Dancing Animals")) || 
				(choosen2.equals("2") && adaAtraksi("Hamster", "Counting Masters")) ||
				(choosen2.equals("3") && adaAtraksi("Hamster", "Passionate Coders"))) {
					System.out.println("Wow, one more step,");
					buatTiket();
			} else {
				System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else if (choosen.equals("2")) {
			System.out.println("\n---Lion---");
			System.out.println("Attractions by Lion:");
			System.out.println("1. Circle of Fires");
			System.out.print("Please choose your preferred attractions (type the number): ");
			choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Lion", "Circle of Fires")){
				System.out.println("Wow, one more step,");
				buatTiket();
			} else {
				System.out.println("Unfortunately, no lion can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else if (choosen.equals("3")) {
			System.out.println("\n---Cat---");
			System.out.println("Attractions by Cat:");
			System.out.println("1. Dancing Animals\n2. Passionate Coders");
			System.out.print("Please choose your preferred attractions (type the number): ");
			choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Cat", "Dancing Animals") || 
				choosen2.equals("2") && adaAtraksi("Cat", "Passionate Coders")) {
				System.out.println("Wow, one more step,");
				buatTiket();
			} else {
				System.out.println("Unfortunately, no cat can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else if (choosen.equals("4")) {
			System.out.println("\n---Whale---");
			System.out.println("Attractions by Whale:");
			System.out.println("1. Circle of Fires\n2. Counting Masters");
			System.out.print("Please choose your preferred attractions (type the number): ");
			choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Whale", "Circle of Fires") || 
				choosen2.equals("2") && adaAtraksi("Whale", "Counting Masters")) {
				System.out.println("Wow, one more step,");
				buatTiket();
			} else {
				System.out.println("Unfortunately, no whale can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else  {
			introJavariPark();
		}
	}

	public void chooseAves(){
		Scanner in = new Scanner(System.in);
		System.out.println("\n--World of Aves--\n1. Eagle\n2. Parrot");
		System.out.print("Please choose your preferred animals (type the number): ");
		String choosen = in.nextLine();
		if (choosen.equals("1")) {
			System.out.println("\n---Eagle---");
			System.out.println("Attractions by Eagle:");
			System.out.println("1. Circle of Fires");
			System.out.print("Please choose your preferred attractions (type the number): ");
			String choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Eagle", "Circle of Fires")) {
					System.out.println("Wow, one more step,");
					buatTiket();
			} else {
				System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else if (choosen.equals("2")) {
			System.out.println("\n---Parrot---");
			System.out.println("Attractions by Parrot:");
			System.out.println("1. Dancing Animals\n2. Counting Masters");
			System.out.print("Please choose your preferred attractions (type the number): ");
			String choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Parrot", "Circle of Fires")) {
					System.out.println("Wow, one more step,");
					buatTiket();
			} else {
				System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else {
			introJavariPark();
		}
	}

	public void chooseReptilles(){
		Scanner in = new Scanner(System.in);
		System.out.println("\n--Reptillian Kingdom--\n1. Snake");
		System.out.print("Please choose your preferred animals (type the number): ");
		String choosen = in.nextLine();
		if (choosen.equals("1")) {
			System.out.println("\n---Snake---");
			System.out.println("Attractions by Hamster:");
			System.out.println("1. Dancing Animals\n2. Passionate Coders");
			System.out.print("Please choose your preferred attractions (type the number): ");
			String choosen2 = in.nextLine();
			if(choosen2.equals("1") && adaAtraksi("Snake", "Dancing Animals") || 
				choosen2.equals("2") && adaAtraksi("Snake", "Passionate Coders") ) {
					System.out.println("Wow, one more step,");
					buatTiket();
			} else {
				System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
				chooseMammals();
			}
		} else {
			introJavariPark();
		}
	}

	public boolean adaAtraksi(String tipe, String atraksi) {
		for (int i=0 ; i<8 ; i++) {
		 	if (i % 3 == 2) {
		 		return true;
		 	}
		}
		return false;
	}

	public void buatTiket(){
		Scanner in = new Scanner(System.in);
	}
}


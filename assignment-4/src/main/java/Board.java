/**
* @Creator : Selvy Fitriani
* @Date : 18 Mei 2018
* tujuan : untuk TP 4 DDP 2 Java
*/


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class Board extends JFrame{      
    /**
    * Card selectedCard adalah kartu temporari yang di-klik saat ini 
    */
    private Card selectedCard;

    /**
    *  Card c1 adalah card yang di-klik pertana
    */
    private Card c1;

    /**
    *  Card c2 adalah card yang di-klik pertana
    */
    private Card c2;

    /**
    *  Timer t1 untuk men-delay proses penutupan kartu
    */
    private Timer t;

    /**
    * Integer pairs, jumlah pasangan kartu yang sama di dalam board 6x6
    */
    final Integer pairs = 18;
   
    /**
    * Integer counter, untuk menghitung berapa kali user menekan card;
    */
    private int counter = 0;

    /**
    * ArrayList<ImageIcon> icons, adalah arraylist dari gambar-gambar yang akan dimasukkan kedalam card
    */
    ArrayList<ImageIcon> icons = new ArrayList<ImageIcon>();

    /**
    * ArrayList<Card> cards, adalah arraylist dari kartu-kartu yang akan dimasukkan ke dalam jPanel
    */
    ArrayList<Card> cards = new ArrayList<Card>(); 

  	JFrame frame = new JFrame();
    JPanel cardPanel = new JPanel();
    JPanel optionPanel = new JPanel();
    JTextField text = new JTextField ("0", 3);
    
    /**
    * @constructor
    */
    public Board(){

        setTitle("Memory Match");
        
        JButton playAgain = new JButton("Play Again?");
        playAgain.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae){
                restartGame();
            }
        });

        JButton exit = new JButton("Exit");
        exit.addActionListener (new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae){
                System.exit(0);
            }
        });

        cardPanel.setLayout(new GridLayout(6,6));
        cardPanel.setBackground(Color.PINK);
        optionPanel.setLayout(new FlowLayout());

        makeIcons();
        Collections.shuffle(icons);
        makeCards();
        
        for (Card c : cards){
            cardPanel.add(c);
        }

        JLabel label = new JLabel("Number of Tries: ");
        
        optionPanel.add(playAgain);
        optionPanel.add(exit);
        optionPanel.add(label);
        optionPanel.add(text);
        add(cardPanel); 
        add(optionPanel, BorderLayout.PAGE_END);
        

        t = new Timer(500, new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae){
                checkCards();
                counter++;
                text.setText(Integer.toString(counter));
            }
        });

        t.setRepeats(false);


    }


    /**
    * @method makeIcons(), untuk menambahkan gambar-gambar ke arraylist.
    */    

    public void makeIcons(){
        String path = System.getProperty("user.dir") + "/Gambar/i"; 
        for (int i = 1; i <= pairs; i++){
            ImageIcon a = new ImageIcon(path + i + ".jpg");
            Image img = a.getImage().getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH); 
            ImageIcon b = new ImageIcon(img);
            b.setDescription(a.getDescription());
            icons.add(b);
            icons.add(b);
        }
    }

    /**
    * @method makeCards()
    * Membuat card dengan icon sebagai clickedIcon
    * Masing masing card akan dilengkapi dengan sebuah method yaitu addActionListener ketika card di-klik 
    * Memasukkan objek card ke ArrayList of card
    */

    public void makeCards(){
        for (ImageIcon icon : icons){
            Card c = new Card(icon, icon.getDescription());
            c.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    selectedCard.clickButton();
                    doTurn();
                }
            });
            cards.add(c);
        }
    }


    /**
    * @method doTurn() melakukan aksi ketika suatu card di klik
    */
    public void doTurn(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
        } else if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            t.start();
        }
    }
       

    /**
    * @method chechCards untuk mengecek apakah dua kartu mengandung objek yang sama atau tidak.
    */
    public void checkCards(){
        if (c1.getId().equals(c2.getId())) {
            c1.setVisible(false);
            c2.setVisible(false);
            c1.setMatch(true);
            c2.setMatch(true);       
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!");
                System.exit(0);
            }
        } else {
            c1.start();
            c2.start();
        }
        c1 = null;
        c2 = null;
    }

    /**
    * @method isGameWon, method untuk menentukan apakah permainan sudah berakhir atau belum
    */
    public boolean isGameWon(){
        for(Card c: cards){
            if (c.getMatch() == false){
                return false;
            }
        }
        return true;
    }

    public void restartGame(){
    	cardPanel.removeAll();
    	counter = 0 ;
    	text.setText(Integer.toString(counter));		
    	icons.clear();
    	cards.clear();
    	makeIcons();
    	Collections.shuffle(icons);
    	makeCards();
    	for (Card c : cards){
            cardPanel.add(c);
        }
        add(cardPanel);
        setVisible(true);
    }
}
import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
* @Creator : Selvy Fitriani
* @Date : 18 Mei 2018
* tujuan : untuk TP 4 DDP 2 Java
*/

public class Card extends JButton{
    /**
    * ImageIcon clickedIcon gambar yang akan dicari pasangannya. 
    */
    private ImageIcon clickedIcon;

    /**
    * boolean match boolean untuk mengecek apakah kartu sudah mendapatkan pasangannya atau belum
    */
    private boolean match;

    /**
    * String id id dari clickIcon yang diperoleh dari directory tempat gambar berada
    */
    private String id;
    
    /**
    * ImageIcon startIcon icon yang pertama kali muncul ketika permainan dimulai
    */
    private final ImageIcon startIcon = new ImageIcon(System.getProperty("user.dir") + "/start.jpg");

    /**
     * Constructs
     *
     * @param ImageIcon icon, adalah gambar yang akan dicari pasangannya
     * @param String id, id yang diperoleh dari directory lokasi icon
     * @param gender gender of animal (male/female)
     */
    public Card(ImageIcon clickedIcon, String id){
        this.clickedIcon = clickedIcon;
        this.id = id;
        start();
    }

    /**
    * @method setId, untuk menge-set id.
    * @param String id, id yang diperoleh dari directory lokasi icon
    */
    public void setId(String id){
        this.id = id;
    }

    /**
    * @method getId, untuk mendapatkan id
    * @return String id, mengembalikan id dengan tipe string 
    */
    public String getId(){
        return this.id;
    }

    /**
    * @method setMatch, untuk menge-set nilai dari boolean Match
    * @param boolean Match, param ini yang akan di assign ke Match yang lama
    */
    public void setMatch(boolean match){
        this.match = match;
    }

    /**
    * @method getMacthed, aksesor untuk boolean Match
    * @return boolean match, untuk mengembalikan nilai dari boolean match
    */
    public boolean getMatch(){
        return this.match;
    }

    /**
    * @method start, untuk me-assign icon dengan startIcon dan untuk 
    *                dan untuk mengaktifkan fungsi klik
    */
    public void start(){
        setIcon(startIcon);
        setEnabled(true);
    }

    /**
    * @method clickButton, untuk me-assign icon dengan button yang di klik saat ini
    */
    public void clickButton(){
        setIcon(clickedIcon);
    }

    /**
    * @method same Icon, untuk me-assign icon dengan sameIcon (icon ketika dua kartu sama)
    *                    Kartu dengan icon yang sama tidak bisa di klik lagi
    *                    Masing-masing dari dua kartu akan di set match nya menjadi true
    */
    
}

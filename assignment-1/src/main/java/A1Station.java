import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int jumlah_kucing = Integer.parseInt(input.nextLine());

        TrainCar now = null;
        TrainCar next = null;
        int counter_a = 0;
        int counter_b = 0;

        for (int i = 1; i <= jumlah_kucing; i++) {
            String kucing = input.nextLine();
            String[] spek_kucing = kucing.split(",");
            WildCat objek_kucing = new WildCat(spek_kucing[0], Double.parseDouble(spek_kucing[1]), Double.parseDouble(spek_kucing[2]));

            if (now == null) {
                now = new TrainCar(objek_kucing);
            } else {
                next = now;
                now = new TrainCar(objek_kucing, next);
            }

            counter_a += 1;
            counter_b += 1;

            if (now.computeTotalWeight() > 250 || counter_b == jumlah_kucing) {
                System.out.print("The train departs to Javari Park" + "\n" + "[LOCO]<--");
                now.printCar();
                System.out.printf("\n" + "Average mass index of all cats: %,.2f%n", now.computeTotalMassIndex() / counter_a);
                System.out.print("In average, the cats in the train are *");
                printMassIndex(now.computeTotalMassIndex() / counter_a);
                System.out.print("*" + "\n");
                now = null;
                counter_a = 0;
            }
        }
    }


    public static void printMassIndex(double totalMassIndex) {
        if (totalMassIndex < 18.5) {
            System.out.print("underweight");
        } else if (totalMassIndex >= 18.5 && totalMassIndex < 25) {
            System.out.print("normal");
        } else if (totalMassIndex >= 25 && totalMassIndex < 30) {
            System.out.print("overweight");
        } else if (totalMassIndex >= 30) {
            System.out.print("obese");
        }
    }
}
 // TODO Complete me!






    }
}

        // TODO Complete me!






    }
}

